{
  const {
    html,
  } = Polymer;
  /**
    `<cells-gema-list-documents>` Description.

    Example:

    ```html
    <cells-gema-list-documents></cells-gema-list-documents>
    ```

    ## Styling
    The following custom properties and mixins are available for styling:

    ### Custom Properties
    | Custom Property     | Selector | CSS Property | Value       |
    | ------------------- | -------- | ------------ | ----------- |
    | --cells-fontDefault | :host    | font-family  |  sans-serif |
    ### @apply
    | Mixins    | Selector | Value |
    | --------- | -------- | ----- |
    | --cells-gema-list-documents | :host    | {} |

    * @customElement
    * @polymer
    * @extends {Polymer.Element}
    * @demo demo/index.html
  */
  class CellsGemaListDocuments extends Polymer.mixinBehaviors([ CellsBehaviors.i18nBehavior, ], Polymer.Element) {
    static get is() {
      return 'cells-gema-list-documents';
    }

    static get properties() {
      return {
        documents: {
          type: Array,
          value: ()=>[],
          notify: true
        },
        tsec: {
          type: String
        },
        loading: {
          type: Boolean,
          value: false
        },
        mysrc: {
          type: String
        },
        url: {
          type: String
        },
        host: {
          type: String
        },
        path: {
          type: String
        },
        params: {
          type: String
        },
        body: {
          type: String
        },
        method: {
          type: String
        },
        headers: {
          type: String
        },
        handleAs: {
          type: String
        }
      };
    }

    static get template() {
      return html `
      <style include="cells-gema-list-documents-styles cells-gema-list-documents-shared-styles"></style>
      <slot></slot>
      <div class="container" hidden$="[[loading]]">
        <template is="dom-repeat" items="[[documents]]" as="document">
          <p><bbva-list-link-icon icon-left="coronita:pdf" icon-right="" on-click="_handlerEvent" data-information$="[[document.information]]" data-heading$="[[document.heading]]">[[document.heading]]</bbva-list-link-icon></p> 
        </template>
        <cells-gema-list-documents-dm
        host="[[host]]"
        path="[[path]]"
        params="[[params]]"
        body="[[body]]"
        method="[[method]]"
        headers="[[headers]]"
        on-granting-ticket-event="grantingTicketEvent"
        on-end-point-get-event="endPointGetEvent"
        on-end-point-event="endPointEvent"
        ></cells-gema-list-documents-dm>
      </div>
      <div hidden$="[[!loading]]" id="Docpdf">
        <cells-file-reader src="{{mysrc}}" is-pdf></cells-file-reader>
      </div>
      `;
    }

    ready() {
      super.ready();
      this._loadData();
    }
    

    _loadData() {
      this.host = 'https://t-glomo-mx.prev.grupobbva.com/';
      this.path= 'QSRV_A02/TechArchitecture/mx/grantingTicket/V02';
      this.body= '{ "authentication": { "userID": "0017803002571691ADMINF", "consumerID": "10000076", "authenticationType": "02", "authenticationData": [{ "idAuthenticationData": "password", "authenticationData": [ "password00"] }] }, "backendUserRequest": { "userId": "ADMINF", "accessCode": "0017803002571691", "dialogId": "" }}';
      this.handleAs = "json";
      console.log('dispara')
      let myDm = Polymer.dom(this.root).querySelector('cells-gema-list-documents-dm');
      myDm.grantingTicket(this.path, this.body, this.handleAs);
    }

    grantingTicketEvent(e){
      if(e.detail.status == 'success'){
        console.log('ok');
        console.log(e)
        this.path='SRVS_A02/business-documents/v0/business-documents';
        this.tsec=e.detail.data.__data.response.headers.tsec;
        this.params='{"product.id":"MyBusiness"}'
        this.handleAs = "json";
        let myDm = Polymer.dom(this.root).querySelector('cells-gema-list-documents-dm');
        myDm.endPointGet(this.path, this.params, this.tsec, this.handleAs);
      }else{
        console.log('error')
      } 
    }

    endPointGetEvent(e){
      if(e.detail.status == 'success'){
        domBind.body='{ "documentTemplate": { "id": "TyC", "templateType": { "id": "PDF" } }, "businessCode": { "documentCodeId": "subproducto", "documentCodevalue": "TyC", "documentProcessId": "proceso", "documentProcessValue": "UGRM07J" }, "documentInfo": { "consumerId": "nombreCanal", "consumerValue": "MyBusiness" }, "metadata": [{ "name": "VERSION", "value": "GMDTCMB001" }] }'
        domBind.handleAs = "text";
        console.log(e.detail.data.__data.response.data);
      }else{
        console.log('error')
      } 
    }

    _handlerEvent(e){
      let myDm = Polymer.dom(this.root).querySelector('cells-gema-list-documents-dm');
      myDm.endPoint(domBind.body, domBind.handleAs);
    }

    endPointEvent(e){
      console.log(e.detail.data.__data.response)
      if(e.detail.status == 'success'){
        let data = e.detail.data.__data.response;
        let mydata = data.substr(data.indexOf('>'), data.lastIndexOf('<metadata>'));
          mydata = mydata.substr(1, mydata.indexOf('--') - 1);
          /*decodificación*/
          let binary = atob(mydata);
          let buffer = new ArrayBuffer(binary.length);
          let view = new Uint8Array(buffer);
          for (let i = 0; i < binary.length; i++) {
            view[i] = binary.charCodeAt(i);
          }
          //Creación objeto Blob
          let blob = new Blob([ view ], { type: 'application/pdf' });
          let url = URL.createObjectURL(blob);
          //Descarga forzada
          let a = document.createElement('a');
          Polymer.dom(this.$.Docpdf).appendChild(a);
          a.setAttribute('href', url);
          a.setAttribute('download', 'file');
          a.click();
           this.mysrc = url; //bindeo a atributo src de cells-file-reader
          // Polymer.dom(this.$.Docpdf).removeChild(a);
          this.loading = true; //muestra visor
      }else{
        console.log('error')
      } 
    }


  }

  customElements.define(CellsGemaListDocuments.is, CellsGemaListDocuments);
}